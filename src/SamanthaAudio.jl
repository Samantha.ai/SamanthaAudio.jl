__precompile__(true)

module SamanthaAudio

using Reexport
@reexport using Samantha
@reexport using SamanthaOptimizer
@reexport using WaveSimulator
@reexport using OnlineStats

include("audio-env.jl")
include("recipes.jl")

export SimulatedAudioEnvironment
export create_audio_server

#=
@everywhere begin
  using SamanthaOptimizer
  global const PATCH_CLAMP_ENABLE = false
  using Samantha
  using OnlineStats
  ccall(:jl_exit_on_sigint, Void, (Cint,), 0)
end
=#

#=
DATA_PATH = "/samantha/data/simulations"
SIM_PATH = joinpath(DATA_PATH, "simulation_" * string(now()))
=#

function create_audio_server(;sim_path=nothing)
  if sim_path == nothing
    if haskey(ENV, "SAMANTHA_AUDIO_SIMULATION_PATH")
      sim_path = ENV["SAMANTHA_AUDIO_SIMULATION_PATH"]
    else
      sim_path = mktempdir()
    end
  end
  mkpath(sim_path)
  objective_path = joinpath(@__DIR__, "objective.jl")
  seed_path = joinpath(@__DIR__, "seed_agent.jl")
  OptimizationServer(objective_path, seed_path) do server, added, deleted
    # TODO: Do something more useful here
    info("Frontier: ", frontier_size(server.frontier), " +", frontier_size([added]), " -", frontier_size(deleted))
  end
end

end
