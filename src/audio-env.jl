struct SimulatedAudioEnvironment{Sim<:Simulator,St<:WaveSimulator.State,D<:WaveSimulator.Domain,B<:WaveSimulator.Backend}
  simulator::Sim
  state::St
  domain::D
  backend::B
end
function SimulatedAudioEnvironment(wave_dims::Int, room_shape::Tuple; fmax=2e3, gamma=0.05, microphones=[], speakers=[])
  resource = CPU1()

  @assert length(room_shape) == wave_dims "Wave dimensions do not match room dimensions"
  wave = UniformWave{wave_dims}(; fmax=fmax)
  domain = BoxDomain(room_shape...; gamma=gamma)

  hooks = []
  for mic_def in microphones
    if mic_def isa PointMicrophone
      push!(hooks, mic_def)
    else
      error("Invalid microphone definition: $(typeof(mic_def))")
    end
  end
  for spk_def in speakers
    if spk_def isa PointSpeaker
      push!(hooks, spk_def)
    else
      error("Invalid speaker definition: $(typeof(spk_def))")
    end
  end
  #mic = PointMicrophone(1, 2.5)
  #spk = PointSpeaker((5, 2.5), rand(Float64, 1024))

  sim = Simulator(wave, hooks...; resource=resource, duration=0.1)
  backend = WaveSimulator.backend_init(sim.resource, domain, sim)
  f0 = WaveSimulator.gauss(domain)
  state = WaveSimulator.state_init(f0, backend, domain, sim)

  SimulatedAudioEnvironment(sim, state, domain, backend)
end

update!(env::SimulatedAudioEnvironment) =
  WaveSimulator.update!(env.state, env.backend, env.simulator)

# FIXME: getters/setters for mics/speakers
# FIXME: Indexing for wave pressure
