using SamanthaAudio

(seed_agent, iface) -> begin
  # TODO: Generate input data?

  return Objective((
    :objective_silence,
    :objective_variance
  ), function opt_func(params)
    agent = deepcopy(seed_agent)
    oldparams = params
    params = deepcopy(params)
    try
      setoptparams!(agent, params)
    catch err
      info(length(getoptparams(seed_agent)[1]))
      for cont in values(agent.nodes)
        info(typeof(root(cont)))
      end
      rethrow(err)
    end

    # Create microphones and speakers
    mic1 = PointMicrophone(1, 2.5)
    #spk1 = PointSpeaker((5, 2.5), CallbackSignal(sample->()))

    # Create SimulatedAudioEnvironment
    env = SimulatedAudioEnvironment(2, (5, 5); microphones=[mic1])#, speakers=[spk1])
    
    # FIXME: Do the thing!
    n_in1_l = iface[:input1]
    n_out1_l = iface[:output1]
    #=
    # Training Phase
    for t in 1:10
      for seq in seqs
        # Allow agent to settle
        for i in 1:10
          run!(agent)
        end

        # Apply sequence
        for i in 1:10
          root(agent.nodes[n_in_l]).state.I .= 20f0 .* seq
          run!(agent)
        end
      end
    end

    #= Objectives
    Silence during non-input
    Activity during input
    Consistency of chosen outputs
    TODO: Sharp partitioning of chosen outputs
    =#

    # Testing Phase
    cm_silence = Series(CountMap(Bool))
    cm_activity = Series(CountMap(Bool))
    fm_seq = [fit!(Series(FitMultinomial(32)), fill(1.0, 32)) for idx in 1:length(seqs)]
    for (idx,seq) in enumerate(seqs)
      # Allow agent to settle
      for i in 1:50
        run!(agent)
        @pc_dump_agent_spikes agent iface
        @pc "Phase" :settle
      end

      # Check for silence
      for i in 1:30
        run!(agent)
        fit!(cm_silence, root(agent.nodes[n_exc_l]).state.F)
        @pc_dump_agent_spikes agent iface
        @pc "Phase" :silence
      end

      # Apply sequence
      for i in 1:10
        root(agent.nodes[n_in_l]).state.I .= 20f0 .* seq
        run!(agent)
        fit!(cm_activity, root(agent.nodes[n_exc_l]).state.F)
        fit!(fm_seq[idx], root(agent.nodes[n_exc_l]).state.F)
        @pc_dump_agent_spikes agent iface
        # FIXME: @pc("Phase", Symbol("sequence_" * string(i)))
      end
    end

    # Return objective evaluations
    prob_silence = probs(stats(cm_silence)[1], [false,true])[2]
    dist_activity = abs(1/length(seqs) - probs(stats(cm_activity)[1], [false,true])[2])
    var_output = sum([var(last(value(first(stats(fm))))) for fm in fm_seq])
    =#
    #=
    info(prob_silence, " | ", dist_activity, " | ", var_output)
    if isnan(var_output)
      vals = Float64[]
      for fm in fm_seq
        push!(vals, var(last(value(first(stats(fm))))))
      end
      info(vals)
      error("NaN")
    end
    =#

    # FIXME: Return something useful
    return (0.0,0.0)
  end)
end
