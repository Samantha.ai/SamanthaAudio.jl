using RecipesBase

@recipe function plot(sae::SimulatedAudioEnvironment)
  heatmap(sae.state.current)
end
