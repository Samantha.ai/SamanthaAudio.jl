seed_agent = Agent()
iface = Dict{Symbol,String}()

n_in1 = GenericNeurons(16)
iface[:input1] = n_in1_l = addnode!(seed_agent, n_in1)
n_out1 = GenericNeurons(16)
iface[:output1] = n_out1_l = addnode!(seed_agent, n_out1)
# TODO: Output for modulation purposes? (Outer Hair Cells)

SeedAgentWrapper(seed_agent, iface)
